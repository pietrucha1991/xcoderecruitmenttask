package com.xCode.xCodeRecruitmentTask.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rate {
    private String no;
    private String effectiveDate;
    private double mid;
}
