package com.xCode.xCodeRecruitmentTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XCodeRecruitmentTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(XCodeRecruitmentTaskApplication.class, args);
	}

}
