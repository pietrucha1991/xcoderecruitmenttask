package com.xCode.xCodeRecruitmentTask.service;

import com.xCode.xCodeRecruitmentTask.dto.inputDto.InputDto;
import com.xCode.xCodeRecruitmentTask.dto.outputDto.OutputDto;
import com.xCode.xCodeRecruitmentTask.exception.CurrencyInputException;
import com.xCode.xCodeRecruitmentTask.exception.NumberInputException;
import com.xCode.xCodeRecruitmentTask.service.model.OrderType;
import com.xCode.xCodeRecruitmentTask.setup.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ApiServiceTest extends IntegrationTest {
    private static final List<Integer> NUMBERS_TO_SORT = new ArrayList<>(List.of(1, 5, 3, 6, 1, 3));
    @Autowired
    ApiService apiService;

    @Test
    void shouldSortNumbersAscending() {
        //when
        InputDto inputDto = InputDto.builder()
                .numbersToSort(NUMBERS_TO_SORT)
                .order(OrderType.ASC)
                .build();

        //given
        OutputDto outputDto = apiService.getSortedNumbers(inputDto);

        //then
        assertEquals(outputDto.getSortedNumbers(), List.of(1, 1, 3, 3, 5, 6));
    }

    @Test
    void shouldSortNumbersDescending() {
        //when
        InputDto inputDto = InputDto.builder()
                .numbersToSort(NUMBERS_TO_SORT)
                .order(OrderType.DSC)
                .build();

        //given
        OutputDto outputDto = apiService.getSortedNumbers(inputDto);

        //then
        assertEquals(outputDto.getSortedNumbers(), List.of(6, 5, 3, 3, 1, 1));
    }

    @Test
    void shouldGetCurrentCurrencyValue() {
        //when
        String currencyCode = "EUR";

        //given
        double currencyValue = apiService.getCurrentCurrencyValue(currencyCode);

        //then
        assertThat(currencyValue).isGreaterThan(0);
    }

    @ParameterizedTest
    @ValueSource(strings = {"EUR", "eur", "USD"})
    void shouldReturnExchangeRate(String input) {
        //given, when
        double currencyValue = apiService.getCurrentCurrencyValue(input);

        //then
        assertThat(currencyValue).isGreaterThan(0);
    }

    @ParameterizedTest
    @ValueSource(strings = {"dummy", "123"})
    void shouldThrowExceptionWhenInvalidInput(String input) {
        //given, when, then
        assertThatThrownBy(() -> apiService.getCurrentCurrencyValue(input)).isInstanceOf(CurrencyInputException.class);
    }

    @Test
    void shouldThrowExceptionWhenOrderTypeIsNull() {
        //when
        InputDto inputDto = InputDto.builder()
                .numbersToSort(NUMBERS_TO_SORT)
                .order(null)
                .build();

        //given, then
        assertThatThrownBy(() -> apiService.getSortedNumbers(inputDto))
                .isInstanceOf(NumberInputException.class)
                .hasMessage("You need to state order");
    }

    @Test
    void shouldThrowExceptionWhenListIsEmpty() {
        //when
        InputDto inputDto = InputDto.builder()
                .numbersToSort(new ArrayList<>())
                .order(OrderType.ASC)
                .build();

        //given, then
        assertThatThrownBy(() -> apiService.getSortedNumbers(inputDto))
                .isInstanceOf(NumberInputException.class)
                .hasMessage("Please provide numbers");
    }

    @Test
    void shouldThrowExceptionWhenListIsNull() {
        //when
        InputDto inputDto = InputDto.builder()
                .order(OrderType.ASC)
                .build();

        //given, then
        assertThatThrownBy(() -> apiService.getSortedNumbers(inputDto))
                .isInstanceOf(NumberInputException.class)
                .hasMessage("Please provide numbers");
    }


}
