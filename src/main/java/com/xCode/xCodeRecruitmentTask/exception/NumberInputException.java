package com.xCode.xCodeRecruitmentTask.exception;

public class NumberInputException extends RuntimeException {

    public NumberInputException(String message) {
        super(message);
    }
}
