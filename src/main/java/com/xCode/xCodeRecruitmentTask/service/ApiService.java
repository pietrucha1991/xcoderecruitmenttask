package com.xCode.xCodeRecruitmentTask.service;

import com.xCode.xCodeRecruitmentTask.dto.inputDto.InputDto;
import com.xCode.xCodeRecruitmentTask.dto.outputDto.OutputDto;
import com.xCode.xCodeRecruitmentTask.exception.CurrencyInputException;
import com.xCode.xCodeRecruitmentTask.exception.NumberInputException;
import com.xCode.xCodeRecruitmentTask.service.model.CurrencyResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Currency;

@Service
@RequiredArgsConstructor
public class ApiService {

    private final static String TEMPLATE_URL = "http://api.nbp.pl/api/exchangerates/rates/a/";
    private final RestTemplate restTemplate;

    public OutputDto getSortedNumbers(InputDto inputDto) {
        validateInput(inputDto);

        if (inputDto.getNumbersToSort().size() == 1) {
            return OutputDto.builder().sortedNumbers(inputDto.getNumbersToSort()).build();
        }

        sort(inputDto);

        return OutputDto.builder().sortedNumbers(inputDto.getNumbersToSort()).build();
    }

    private void validateInput(InputDto inputDto) {
        if (inputDto.getNumbersToSort() == null || inputDto.getNumbersToSort().isEmpty()) {
            throw new NumberInputException("Please provide numbers");
        }
        if (inputDto.getOrder() == null) {
            throw new NumberInputException("You need to state order");
        }
    }

    private void sort(InputDto inputDto) {
        switch (inputDto.getOrder()) {
            case ASC -> Collections.sort(inputDto.getNumbersToSort());
            case DSC -> inputDto.getNumbersToSort().sort(Collections.reverseOrder());
        }
    }

    public double getCurrentCurrencyValue(String currencyCode) {
        Currency currency = parse(currencyCode);

        String targetUrl = TEMPLATE_URL + currency.getCurrencyCode();

        ResponseEntity<CurrencyResponse> responseEntity = restTemplate.getForEntity(targetUrl, CurrencyResponse.class);

        return responseEntity.getBody().getRates().get(0).getMid();
    }

    private Currency parse(String currencyCode) {
        try {
            return Currency.getInstance(currencyCode.toUpperCase());
        } catch (Exception e) {
            throw new CurrencyInputException("Value " + currencyCode + "not supported");
        }
    }
}
