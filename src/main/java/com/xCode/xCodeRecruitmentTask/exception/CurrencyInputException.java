package com.xCode.xCodeRecruitmentTask.exception;

public class CurrencyInputException extends RuntimeException {
    public CurrencyInputException(String message) {
        super(message);
    }
}
