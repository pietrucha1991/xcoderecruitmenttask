package com.xCode.xCodeRecruitmentTask.controller;

import com.xCode.xCodeRecruitmentTask.dto.inputDto.InputDto;
import com.xCode.xCodeRecruitmentTask.dto.outputDto.OutputDto;
import com.xCode.xCodeRecruitmentTask.exception.CurrencyInputException;
import com.xCode.xCodeRecruitmentTask.service.ApiService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class ApiController {

    private final ApiService apiService;


    @GetMapping("/status/ping")
    public String getPong() {
        return "pong";
    }

    @PostMapping("numbers/sort-command")
    @CrossOrigin("http://localhost:4200")
    public OutputDto sortNumbers(@RequestBody InputDto inputDto) {
        return apiService.getSortedNumbers(inputDto);
    }

    @PostMapping("/currencies/get-current-currency-value-command")
    public double getCurrencyValue(@RequestBody String currencyCode) throws CurrencyInputException {
        return apiService.getCurrentCurrencyValue(currencyCode);
    }


}
