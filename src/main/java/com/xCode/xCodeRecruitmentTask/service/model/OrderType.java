package com.xCode.xCodeRecruitmentTask.service.model;

public enum OrderType {
    ASC, DSC
}
