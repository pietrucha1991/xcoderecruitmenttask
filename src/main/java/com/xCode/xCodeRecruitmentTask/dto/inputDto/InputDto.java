package com.xCode.xCodeRecruitmentTask.dto.inputDto;

import com.xCode.xCodeRecruitmentTask.service.model.OrderType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InputDto {
    List<Integer> numbersToSort;
    OrderType order;
}
